﻿/**
 * Pig with Lipstick -- an HTML5 game
 * Roll the die first one to reach winning score wins.
 *
 * MIT License:
 * Copyright (C) 2013 Michael Keller.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */
// global directives
/*global jPrompt */
/*global jAlert */


(function () {
// Setup canvas and context
    var VERSION = '1.6.0',
        WINNING_SCORE = 20,
        CPU_MIN_ROLLS = 2,
        CPU_MAX_ROLLS = 5,
        MS_MIN_BETWEEN_CLICKS = 1000,
        AUDIO_VOLUME = 0.5,
        scoreTallied = false,
        gameOver = false,
        canvas = document.getElementById('canvas'),
        context = canvas.getContext('2d'),
        you,
        cpu,
        imageDie = new Image(),
        gameMessages,
        msLastRollTime,
        waitId1 = 0,
        waitId2 = 0,
        pwlYouWins,
        pwlYouLosses;

    function playSound(theSound) {
        'use strict';
        // Stop any sound in progress
        if (theSound[0] !== undefined) {
            theSound[0].pause();
            theSound[0].currentTime = 0;
            // Set volume and play sound
            theSound[0].volume = AUDIO_VOLUME;
            // Needed cause may not play after first time
            theSound[0].load();
            theSound[0].play();
        }
    }

    function randomFromInterval(from, to) {
        //return Math.floor(Math.random() * (to - from + 1) + from);
        return Math.random() * to + from | 0;
    }

    function toTitleCase(str) {
        return str.replace(/\w\S*/g, function (txt) {
            return txt.charAt(0).
                    toUpperCase() + txt.substr(1).toLowerCase();
        });
    }

    /*****************************************************************************
     * Game logic
     */
    function Player(name, displayName, color, x, y) {
        'use strict';
        var str,
            turn,
            score,
            wins;

        this.name = name;
        this.displayName = displayName;
        this.color = color;
        this.x = x;
        this.y = y;
        this.turn = 0;
        this.score = 0;
        this.wins = 0;
        this.lastRoll = 0;
        this.rollsLeft = 0;
        this.gameOverSound = "";
        this.rollsArray = ['1 to 6->', 0, 0, 0, 0, 0, 0];

        this.postScores = function () {
            context.font = 'bold 14pt Consolas';
            context.lineWidth = 0;
            context.fillStyle = 'lightgrey';
            context.fillRect(x, y - 20, 300, 30);
            context.fillStyle = color;
            turn = ('   ' + this.turn).slice(-3);
            score = ('   ' + this.score).slice(-3);
            wins = ('   ' + this.wins).slice(-3);
            str = 'Turn' + turn + '  Score' + score + '  Wins' + wins;
            context.fillText(str, this.x, this.y);
        };
    }

    function GameMessages(x1, y1, width, height) {
        'use strict';
        var MAX_MESSAGES = 7,
            messageBuffer = [],
            i,
            topBuffer,
            len,
            start;

        this.left = x1;
        this.top = y1;
        this.width = width;
        this.height = height;

        this.drawMessageBox = function () {
            context.font = 'normal 12pt Calibri';
            context.lineWidth = 1;
            context.strokeRect(x1, y1, 550, 135);
            context.fillStyle = 'lightyellow';
            context.fillRect(x1, y1, 550, 135);
            context.fillStyle = 'black';
        };

        this.clearMessageBox = function () {
            context.clearRect(x1, y1, width, height);
        };

        this.clearMessageBuffer = function () {
            messageBuffer = [];
        };

        // Adds a  message to end of message buffer
        // Displays last MAX_MESSAGES
        this.addMessage = function (color, arg) {
            this.clearMessageBox();
            this.drawMessageBox();
            messageBuffer.push(arg);
            topBuffer = this.top + 12;
            len = messageBuffer.length;
            start = len < MAX_MESSAGES ? 0 : len - MAX_MESSAGES;
            for (i = start; i < len; i++) {
                if (messageBuffer[i] === undefined) {
                    break;
                }
                context.font = 'normal 12pt Consolas';
                context.fillStyle = color;
                context.fillText(messageBuffer[i], this.left + 2, topBuffer);
                topBuffer += 18;
            }
        };
    }

    function isGameOver() {
        'use strict';

        var winner = you.score >= WINNING_SCORE ? you : cpu.score >= WINNING_SCORE ? cpu : null;
        if (winner !== null) {
            gameOver = true;
            gameMessages.addMessage('lightyellow', " ");
            gameMessages.addMessage(winner.color, winner.displayName + ", " + "has won! ");
            winner.wins += 1;
            winner.postScores();
            if (winner === you) {
                gameMessages.addMessage(you.color, "There is hope for the human race.");
                localStorage.setItem(pwlYouWins, winner.wins.toString());
            } else {
                gameMessages.addMessage(cpu.color, "Humans will now be their slaves.");
                localStorage.setItem(pwlYouLosses, cpu.wins.toString());
            }
            playSound(winner.gameOverSound);
            utils.setButtonState("disable", ["btnRoll", "btnPass"]);
            console.log(you.name + " " + you.rollsArray);
            console.log(cpu.name + " " + cpu.rollsArray);
            return true;
        }
        return false;
    }

    function tallyScores(player) {
        'use strict';
        var id,
            sound,
            scoreToBeTallied = false;

        id = setInterval(function () {
            if (player.turn > 0) {
                scoreToBeTallied = true;
                player.turn -= 1;
                player.score += 1;
                player.postScores();
            }
            if (player.turn === 0) {
                if (scoreToBeTallied) {
                    sound = $('#sndChaChing');
                    playSound(sound);
                    scoreToBeTallied = false;
                }
                isGameOver();
                clearInterval(id);
                scoreTallied = true;
                setTimeout(function () {
                    /*$("#btnRoll").is(":disabled") $("#btnRoll").css({ cursor: 'pointer' });
                     $('#btnPass').css({ cursor: 'pointer' });*/
                }, 1000);
            }
        }, 150);
    }

    function afterRollSounds(player) {
        'use strict';

        if (player.name === 'you' && player.lastRoll === 6) {
            playSound($('#sndWow'));
        }

        if (player.lastRoll === 1) {
            playSound($('#sndOink'));
        }

        if (player.name === 'cpu' && player.lastRoll === 6) {
            playSound($('#sndBoo'));
        }
    }

    function rollDieComplete(player) {
        'use strict';

        afterRollSounds(player);

        if (player.lastRoll === 1) {
            player.turn = 0;
            player.postScores();
            if (player.name === 'you') {
                gameMessages.addMessage(player.color, "You rolled 1 Sorry, " +
                    player.displayName + ", you've lost your Turn points.");
                gameMessages.addMessage(player.color, player.displayName +
                    " passes  to " + cpu.displayName);
                utils.setButtonState("disable", ["btnRoll"]);
            } else {
                gameMessages.addMessage(player.color, player.displayName +
                    " rolled 1 " +
                    "Muhahaha, and loses its turn points.");
                gameMessages.addMessage(player.color,
                    cpu.displayName + " passes to " + you.displayName);
                utils.setButtonState("enable", ["btnRoll"]);
            }
        } else {
            player.turn += player.lastRoll;

            // Dont let cpu beat itself
            if ((player.name === 'cpu') &&
                (player.turn + player.score >= WINNING_SCORE)) {
                player.rollsLeft = 0;
                //console.log('Cpu rolls left 0');
            }
            player.postScores();
            gameMessages.addMessage(player.color, toTitleCase(player.name) +
                " rolled " + player.lastRoll + " " + toTitleCase(player.name) +
                " earned " + ('  ' + player.turn).slice(-2));
            if (player.name === 'you') {
                utils.setButtonState("enable", ["btnRoll"]);
            }
        }

        if (player.name === 'cpu') {
            player.rollsLeft -= 1;
            if (player.lastRoll === 1) {
                /*return;*/
            } else if (player.rollsLeft <= 0) {
                gameMessages.addMessage(cpu.color, "Cpu passes to " +
                    you.displayName);
                // so that you can't force cpu to lose turn by causing it to roll
                // until it rolls a 1
                utils.setButtonState("disable", ["btnPass"]);
                utils.setButtonState("enable", ["btnRoll"]);
            } else {
                setTimeout(function () {
                    rollDie(cpu);
                }, 750);
            }
        }
    }

    function rollDie(player) {
        'use strict';
        var dieColor = player.color,
            dieNum,
            cnt = 0,
            sound = $('#sndRoll');

        scoreTallied = false;
        if (player.name === 'you') {
            tallyScores(cpu);
        } else {
            tallyScores(you);
        }

        waitId1 = setInterval(function () {
            if (scoreTallied && !gameOver) {
                playSound(sound);
                clearInterval(waitId1);
                waitId2 = setInterval(function () {
                    dieNum = randomFromInterval(1, 6);
                    imageDie.src = 'images/' + dieColor + 'Die' + dieNum + '.png';
                    cnt++;
                    if (cnt >= 15) {
                        clearInterval(waitId2);
                        player.lastRoll = dieNum;
                        player.rollsArray[dieNum]++;
                        rollDieComplete(player);
                    }
                }, 50);
            }
        }, 100);
    }

    function initWins() {
        // Initialize the scores and store locally if not already stored
        'use strict';
        pwlYouWins = 'pwl' + you.displayName + 'Wins';
        pwlYouLosses = 'pwl' + you.displayName + 'Losses';
        //console.log(pwlYouWins + '  ' + pwlYouLosses);
        if (localStorage.getItem(pwlYouWins) === null) {
            localStorage.setItem(pwlYouWins, '0');
            localStorage.setItem(pwlYouLosses, '0');
        }
        you.wins = Number(localStorage.getItem(pwlYouWins));
        you.postScores();
        cpu.wins = Number(localStorage.getItem(pwlYouLosses));
        cpu.postScores();
    }

    function ignoreClick() {
        'use strict';
        // Don't let the buttons Roll and Pass be clicked too quickly
        var theDate,
            msNow;

        theDate = new Date();
        msNow = theDate.getTime();
        if (msNow < msLastRollTime + MS_MIN_BETWEEN_CLICKS) {
            return true;
        }
        msLastRollTime = msNow;
        return false;
    }

    /*****************************************************************************
     * Gui
     */
    function showPigPhoto(y) {
        'use strict';
        var image = new Image(),
            photoHeight = 168;

        image.src = 'images/pig_with_lipstick2.png';
        image.onload = function () {
            var x = canvas.width / 2 - image.width / 2;
            context.drawImage(image, x, y);
        };
        return y + photoHeight;
    }

    function drawTextAlongArc(context, str, centerX, centerY, radius, angle) {
        'use strict';
        var len = str.length, s, n;
        context.save();
        context.translate(centerX, centerY);
        context.rotate(-1.05 * angle / 2);
        context.rotate(-1 * (angle / len) / 2);
        for (n = 0; n < len; n++) {
            context.rotate(angle / len);
            context.save();
            // -1 curve upward  1 curve downward
            context.translate(0, radius);
            s = str[n];
            context.fillText(s, 0, 0);
            context.restore();
        }
        context.restore();
    }

    function drawPigCaption() {
        'use strict';
        var centerX, centerY, angle, radius;

        context.save();
        centerX = canvas.width / 2;
        centerY = 45;
        angle = Math.PI * 0.65;
        radius = 170;

        context.font = '20pt Elephant';
        context.textAlign = 'center';
        context.fillStyle = 'red';
        context.strokeStyle = 'blue';
        context.lineWidth = 2;
        drawTextAlongArc(context, '!kcitspiL htiW giP', centerX, centerY,
            radius, angle);

        // draw arc underneath text
        context.arc(centerX, centerY, radius + 5, 0.57, 0.81 * Math.PI, false);
        context.stroke();
        context.restore();
    }

    function drawPlayerLabel(x, y, name) {
        'use strict';
        context.font = 'normal 14pt Consolas';
        context.fillStyle = 'black';
        context.fillText(name, x, y);
        return y;
    }

    function drawPlayerName(x, y, name, color) {
        'use strict';
        context.font = 'bold 14pt Calibri';
        context.lineWidth = 3;
        context.strokeRect(x, y - 20, 210, 30);
        context.fillStyle = 'lightyellow';
        context.fillRect(x, y - 20, 210, 30);
        context.fillStyle = color;
        context.fillText(name, x + 2, y);
        return y;
    }

    function showRules(x, y) {
        'use strict';
        var rules = [],
            i,
            boxHeight;

        rules.push("1. A player rolls a six sided die. Face value is " +
            "added to Turn unless a 1 is rolled.");
        rules.push("2. If a player rolls a 1 the player loses all points " +
            "for that turn and the turn ends.");
        rules.push("3. When a turn ends Turn points are added to Score " +
            "and Turn points are set to 0.");
        rules.push("4. Player rolling the die may pass to the other player " +
            "at any time.");
        rules.push("5. First player to end their turn with a Score of at " +
            "least " + WINNING_SCORE + " Wins.");

        // Rules label
        context.font = 'normal 14pt Calibri';
        context.fillStyle = 'black';
        context.fillText('Rules:', 50, y);

        // Rules box
        context.font = 'normal 12pt Calibri';
        context.lineWidth = 1;
        boxHeight = 22 * rules.length;
        context.strokeRect(x, y - 20, 550, boxHeight);
        context.fillStyle = 'lightyellow';
        context.fillRect(x, y - 20, 550, boxHeight);
        context.fillStyle = 'black';
        for (i = 0; i < rules.length; i++) {
            context.fillText(rules[i], canvas.width / 4 + 2, y);
            y += 20;
        }
    }

    function showAbout() {
        'use strict';

        var str = "Pig With Lipstick version " + VERSION + ".\n\n" +
            "The game is based on a Python console game\n " +
            "written by Joe Scalzo. Mike Keller made this\n" +
            "implementation in HTM5 and JavaScript. \n" +
            "No pigs were harmed in the creation of this game.\n";

        jAlert(str, "About Pig With Lipstick", function () {

        });
    }

    function processButtonClicks(name) {
        "use strict";
        var mouseDownSound;

        // Process button clicks
        switch (name) {
        case 'New Game':
            // Need to clear loop rollDie may have been in during last game.
            gameOver = false;
            scoreTallied = false;
            clearInterval(waitId1);
            clearInterval(waitId2);
            msLastRollTime = new Date();
            msLastRollTime = msLastRollTime.getTime();
            mouseDownSound = $('#sndEngage');
            playSound(mouseDownSound);
            you.turn = 0;
            you.score = 0;
            you.postScores();
            cpu.turn = 0;
            cpu.score = 0;
            cpu.postScores();
            gameMessages.clearMessageBuffer();
            gameMessages.addMessage('black', 'Click Roll to begin');
            utils.setButtonState("enable", ["btnRoll", "btnPass"]);
            break;
        case 'Roll':
            if (ignoreClick()) {
                return;
            }
            you.rollsLeft = 1;
            //console.log("Button Roll has " + you.rollsLeft + " rolls.");
            utils.setButtonState("disable", ["btnRoll"]);
            utils.setButtonState("enable", ["btnPass"]);
            rollDie(you);
            break;
        case 'Pass':
            if (ignoreClick()) {
                return;
            }
            cpu.rollsLeft = randomFromInterval(CPU_MIN_ROLLS, CPU_MAX_ROLLS);
            //console.log("Button Pass has " + cpu.rollsLeft + " rolls.");
            utils.setButtonState("disable", ["btnRoll", "btnPass"]);
            rollDie(cpu);
            break;
        case 'About':
            showAbout();
            break;
        default:
        }
    }

    function loadDie() {
        'use strict';
        var i, j, color, image;

        for (i = 1; i <= 2; i++) {
            color = i === 1 ? 'blue' : 'red';
            for (j = 1; j <= 6; j++) {
                image = new Image();

                image.src = 'images/' + color + 'Die' + j + '.png';
                /*image.onload = console.log('images/' + color + 'Die' + j + '.png');*/
            }
        }
    }

    $('button').on('mousedown', function (event) {
        'use strict';
        var name = event.target.firstChild.data;
        processButtonClicks(name);
    });

    function init() {
        "use strict";
        console.log(Object.keys(window));
        var x = [50, canvas.width / 4, canvas.width / 2 + 30],
            y = [10, 210, 250, 300, 350, 470, 625],
            youName,
            cpuName;

        // Pig photo and caption
        showPigPhoto(y[0]);
        drawPigCaption();

        // You
        youName = 'Human';
        you = new Player('you', youName, 'blue', x[2], y[2]);
        drawPlayerLabel(x[0], y[2], 'Your Name:');
        drawPlayerName(x[1], y[2], you.displayName, you.color);
        you.postScores();
        you.gameOverSound = $('#sndHallelujah');
        jPrompt('Minimum 4 characters', 'Mike', 'Your Name',
            function (r) {
                // !r checks for empty strings (""), null, undefined, false and
                // the numbers 0 and NaN
                if (!r) {
                    window.location.reload(true);
                }
                r = r.trim();
                if (r.length >= 4) {
                    //youName = r;
                    you.displayName = r;
                    drawPlayerName(x[1], y[2], you.displayName, you.color);
                    setTimeout(function () {
                        playSound($('#sndWelcome'));
                        initWins();
                    }, 300);
                }
                else {
                    window.location.reload(true);
                }
            });

        // CPU
        cpuName = 'Cpu';    //' (' + navigator.platform + ')';
        cpu = new Player('cpu', cpuName, 'red', x[2], y[3]);
        drawPlayerLabel(x[0], y[3], 'Opponent:');
        drawPlayerName(x[1], y[3], cpu.displayName, cpu.color);
        cpu.postScores();
        cpu.gameOverSound = $('#sndFutile');

        // Display Rules
        showRules(x[1], y[4]);

        // Game messages
        gameMessages = new GameMessages(x[1], y[5] - 20, 550, 135);
        gameMessages.drawMessageBox();
        gameMessages.addMessage('black', 'Welcome');

        // Init game buttons
        utils.setButtonState("enable", ["btnNewGame", "btnAbout"]);
        utils.setButtonState("disable", ["btnPass", "btnRoll"]);

        // load all die images for quick access
        loadDie();

        imageDie.src = 'images/redDie1.png';
        imageDie.onload = function () {
            context.drawImage(imageDie, x[0], y[6] - 150);
        };
    }

    $(function () {
        init();
    });
})();
